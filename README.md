# frontend

[Link de demonstração.](https://federated-states.herokuapp.com)

[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=celk-exam_frontend&metric=alert_status)](https://sonarcloud.io/dashboard?id=celk-exam_frontend)

Projeto frontend para gerenciamento de Unidades federativas

## Tecnologias

Tecnologias utilizadas no desenvolvimento:

 - AngularJS
 - Webpack
 - Babel
 - Bootstrap
 - Node/npm
 - Docker
 - Gitlab-CI
 - Heroku

## Executando ambiente de desenvolvimento

Para executar o projeto basta realizar o clone do repositório e executar os comandos abaixo:

```bash
$ npm i
$ npm run start
```

> O backedn configurado está apontando para o ambiente executando no endereço `https://federated-states-backend.herokuapp.com`.
> Para apontar para execução local, alterar o arquivo `src/common/config.js`.
