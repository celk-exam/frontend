export default {
    appName: 'FederatedStates',
    api: {
        base: 'https://federated-states-backend.herokuapp.com',
        resources: {
            federatedStates: '/federated-states'
        }
    },
    stateUrls: {
        home: '/',
        about: '/about'
    }
};
