import angular from 'angular';
import template from './federated-state-form.tpl.html'
import federatedStateService from '../../services/federated-state.service';

export default angular.module('federatedStateForm.component', [])
    .factory(federatedStateService.name, federatedStateService.factory)
    .component('federatedStateForm', {
        template,
        controller: ['$rootScope', federatedStateService.name, function ($rootScope, federatedState) {
            const ctrl = this;

            ctrl.state = {code: '', name: ''}
            ctrl.error = null

            ctrl.save = () => {
                federatedState.save(ctrl.state)
                    .then(() => {
                        ctrl.state = {code: '', name: ''}
                        $rootScope.$broadcast('updateFederateStates')
                    })
                    .catch(error => {
                        ctrl.error = error.data.message
                    })
            }

            $rootScope.$on('editFederatedState', function (event, data) {
                ctrl.state = data
            })
        }],
    }).name;
