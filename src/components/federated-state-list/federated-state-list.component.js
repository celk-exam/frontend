import angular from 'angular';
import template from './federated-state-list.tpl.html'
import federatedStateService from '../../services/federated-state.service';

export default angular.module('federatedStateList.component', [])
    .factory(federatedStateService.name, federatedStateService.factory)
    .component('federatedStateList', {
        template,
        controller: ['$rootScope', federatedStateService.name, function ($rootScope, federatedState) {
            const ctrl = this;
            ctrl.states = [];

            ctrl.remove = (id) => {
                federatedState.remove(id)
                    .then(() => {
                        fetchStates();
                    });
            };

            ctrl.edit = (payload) => {
                $rootScope.$broadcast('editFederatedState', payload)
            };

            $rootScope.$on('updateFederateStates', function () {
                fetchStates();
            });

            const fetchStates = () => {
                federatedState.getFederatedStates()
                    .then(result => {
                        ctrl.states = result.content;
                    });
            };

            fetchStates();
        }],
    }).name;
