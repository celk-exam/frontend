import config from '../common/config';

const BASE_URL = config.api.base + config.api.resources.federatedStates;

export default {
    name: 'federatedStateService',
    factory: ['$http', ($http) => {
        function getFederatedStates() {
            return $http.get(`${BASE_URL}?sort=name&direction=ASC`)
                .then(result => result.data)
                .catch(error => error);
        }

        function save(payload) {
            if (payload.id) {
                return $http.put(`${BASE_URL}/${payload.id}`, payload)
            }
            return $http.post(BASE_URL, payload)
        }

        function remove(id) {
            return $http.delete(`${BASE_URL}/${id}`)
                .then(result => result.data)
                .catch(error => error);
        }

        return {
            getFederatedStates,
            save,
            remove,
        };
    }]
};
