import angular from 'angular';
import template from './about.tpl.html';

export default angular.module('about.view', [])
    .component('about', {
        template,
    }).name;
