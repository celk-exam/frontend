import angular from 'angular';
import template from './home.tpl.html';
import federatedStateForm from '../../components/federated-state-form/federated-state-form.component';
import federatedStateList from '../../components/federated-state-list/federated-state-list.component';

export default angular
    .module('home.view', [federatedStateForm, federatedStateList])
    .component('home', {
        template,
    }).name;
