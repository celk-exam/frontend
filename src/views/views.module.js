import angular from 'angular';
import config from '../common/config';

import homeView from './home/home.view';
import aboutView from './about/about.view';

export default angular.module(`${config.appName}.views`, [
    homeView,
    aboutView,
]).name;
